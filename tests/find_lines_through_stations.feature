Feature: Timetable service should find correct lines running through stations

Scenario Outline: Find lines running through stations
    Given I am traveling on the <lineName> line from <lineDeparture>
    When I want to travel from <departure> to <destination>
    Then I should leave <lineName> from <lineDeparture>

    Examples:
    | departure     | destination   | lineName  | lineDeparture |
    | Parramatta  | Town Hall   | Western | Emu Plains |
    | Town Hall   | Parramatta  | Western | North Richmond |
    | Strathfield | Epping      | Epping  | City |