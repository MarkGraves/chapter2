Feature: Provide travelers with optimal itineraries

Scenario: Find arrival times for a station on a line
    Given I am traveling on the Western line from Emu Plains
    When I want to travel to Parramatta
    Then I should be told about the trains at 7:58, 8:00, 8:02, 8:11, 8:14, 8:21