import pytest
import time

from pytest_bdd import given, scenario, then, when

from datetime import time
from resources import Line
from resources import TimetableService
from resources import ItineraryService

@scenario('find_lines_through_stations.feature',
          'Find lines running through stations',
          example_converters=dict(lineName=str, lineDeparture=str,
                                 departure=str, destination=str))
def test_find_lines_through_stations():
    pass

@given('I am traveling on the <lineName> line from <lineDeparture>')
def given_line_named_departing_from(lineName,lineDeparture):
    return dict(line=Line.Line.named(lineName).departingFrom(lineDeparture))


@when('I want to travel from <departure> to <destination>')
def when_i_want_to_travel_to(given_line_named_departing_from, departure,destination):
    given_line_named_departing_from['departure'] = departure
    given_line_named_departing_from['destination'] = destination

@then('I should leave <lineName> from <lineDeparture>')
def should_leave_line_from_origin(given_line_named_departing_from, lineName,lineDeparture):

    departure = given_line_named_departing_from['departure']
    destination = given_line_named_departing_from['destination']

    service = TimetableService.TimetableService()

    proposed_line = service.findLinesThrough(departure,destination)[0]
    expected_line = lineName

    proposed_departure = proposed_line.departingFrom()
    expected_departure = lineDeparture

    assert(proposed_line.lineName == lineName)
    assert(proposed_departure == expected_departure)
