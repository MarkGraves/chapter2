"""Provide travelers with optimal itineraries feature tests."""
import pytest


from datetime import time

from pytest_bdd import given, scenario, then, when

@scenario('calculate_arrival_times.feature','Calculate arrival times',
         example_converters=dict(departure=str, destination=str,
                                 departure_time=str, line=str,
                                 arrival_time=str))
def test_calculate_arrival_times():
    """Calculate arrival times."""
    pass


@given('I want to go from <departure> to <destination>')
def i_want_to_go(departure,destination):
    return dict(_from=departure,_to=destination)

@given('the next train leaves at <departure_time> on <line> line')
def the_next_train_leaves_at(i_want_to_go, departure_time, line):
    """the next train leaves at <departure_time> on <line> line."""
    i_want_to_go['at_time'] = departure_time
    i_want_to_go['on_line'] = line
    return i_want_to_go


@when('I ask for my arrival time')
def i_ask_for_my_arrival_time(i_want_to_go,xline,tt,its):

    lineName = i_want_to_go['on_line']

    departure = i_want_to_go['_from']
    destination = i_want_to_go['_to']

    hours = int(i_want_to_go['at_time'][0])
    minutes = int(i_want_to_go['at_time'][2:3])
    startTime = time(hours,minutes)

    line = xline.Line.named(lineName).departingFrom(departure)

    timetable = tt.TimetableService()

    print line, departure, destination
    allArrivalTimes = timetable.findArrivalTimes(line, destination)

    print allArrivalTimes

    service = its.ItineraryService(timetable)

    proposed_arrival_time = service.findArrivalTimesAfter(startTime, allArrivalTimes)
    print proposed_arrival_time

    i_want_to_go['proposed'] = proposed_arrival_time
    return i_want_to_go

@then('the estimated arrival time should be <arrival_time>')
def the_estimated_arrival_time_should_be_arrivaltime(i_want_to_go, arrival_time):
    """the estimated arrival time should be <arrival-time>."""
    proposed_arrival_time = i_want_to_go['proposed']
    expected_arrival_time = time(int(arrival_time[0]),int(arrival_time[2:4]))
    print proposed_arrival_time

    print expected_arrival_time

    assert(proposed_arrival_time == expected_arrival_time)
