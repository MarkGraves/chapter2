"""Provide travelers with optimal itineraries feature tests."""
import pytest
import time

from pytest_bdd import given, scenario, then, when

from datetime import time
from resources import Line
from resources import TimetableService
from resources import ItineraryService


@scenario('provide_travelers_with_optimal_itineraries.feature', 'Find the optimal itinerary between stations on the same line')
def test_find_the_optimal_itinerary_between_stations_on_the_same_line():
    pass

@given('Western line trains from Emu Plains leave Parramatta to Town Hall at 7:58, 8:00, 8:02, 8:11, 8:14, 8:21')
def given_line_named_departing_from():
    return dict(line=Line.Line.named("Western").departingFrom("Emu Plains"))


@when('I want to travel from Parramatta to Town Hall at 8:00')
def when_i_want_to_travel_on(given_line_named_departing_from):
    given_line_named_departing_from['departure'] = "Parramatta"
    given_line_named_departing_from['destination'] = "Town Hall"
    given_line_named_departing_from['startTime'] = time(8,00)

    pass

@then('I should be told about the trains at 8:02, 8:11, 8:14')
def should_find_these_trains(given_line_named_departing_from):

    expected_departure_times = [time(8,2), time(8,11),time(8,14)]

    timetable = TimetableService.TimetableService()
    itinerary = ItineraryService.ItineraryService(timetable)

    departure = given_line_named_departing_from['departure']
    destination = given_line_named_departing_from['destination']
    startTime = given_line_named_departing_from['startTime']

    proposed_departure_times = itinerary.findNextDepartures(departure, destination, startTime)

    assert(proposed_departure_times == expected_departure_times)


