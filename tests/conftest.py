from resources import TimetableService
from resources import ItineraryService
from resources import Line

import pytest

@pytest.fixture(scope='session',autouse=True)
def xline():
    return Line


@pytest.fixture(scope='module',autouse=True)
def tt():
    return TimetableService

@pytest.fixture(scope='module',autouse=True)
def its():
    return ItineraryService