import pytest
import time

from pytest_bdd import given, scenario, then, when

from datetime import time
from resources import Line
from resources import TimetableService
from resources import ItineraryService

@scenario('find_arrival_times_at_station.feature', 'Find arrival times for a station on a line')
def test_find_arrival_times_at_station():
    pass

@given('I am traveling on the Western line from Emu Plains')
def given_line_named_departing_from():
    return dict(line=Line.Line.named("Western").departingFrom("Emu Plains"))


@when('I want to travel to Parramatta')
def when_i_want_to_travel_to(given_line_named_departing_from):
    given_line_named_departing_from['destination'] = "Parramatta"
    pass

@then('I should be told about the trains at 7:58, 8:00, 8:02, 8:11, 8:14, 8:21')
def should_find_these_times(given_line_named_departing_from):

    expected_departure_times = [time(7,58), time(8,0), time(8,2),
                              time(8,11),time(8,14), time(8,21)]

    destination = given_line_named_departing_from['destination']
    line = given_line_named_departing_from['line']

    service = TimetableService.TimetableService()

    proposed_arrival_times = service.findArrivalTimes(line, destination)

    print proposed_arrival_times, expected_arrival_times

    assert(proposed_arrival_times == expected_arrival_times)


